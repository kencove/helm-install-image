# Tag strategy for Docker image

## Release images

We tag two release images:

1. `helm-${HELM3_VERSION}-kube-${KUBECTL_VERSION}-alpine-${ALPINE_VERSION}`

This is for the dependency updater to update README.md without having knowledge of the next version.

2. `${CI_COMMIT_TAG}-helm-${HELM3_VERSION}-kube-${KUBECTL_VERSION}-alpine-${ALPINE_VERSION}`

This is for downstream projects' dependency updaters which may require a semver-like version.

`CI_COMMIT_TAG` (e.g. `v0.399`) is mainly a hack that we use to help the renovate bot detect when we had changes to the `helm-install-image` that should reflect in version bumps upstream. But this version number has no meaningful use to the user. So, we push the image names without it also. This number is literally the incremental `CI_PIPELINE_IID`.

[This MR comment](https://gitlab.com/gitlab-org/cluster-integration/helm-install-image/-/merge_requests/59#note_1020889478) has more context.

## Release jobs

Release jobs work as below:

- Once an MR gets merged, the last job of the default branch pipeline ([prepare-release](https://gitlab.com/gitlab-org/cluster-integration/helm-install-image/-/blob/f23a3be8c48f50d610c85c455d2a5b1c2af57e53/.gitlab-ci.yml#L214-231)) gets triggered.
- This job calls the [`release-cli`](https://gitlab.com/gitlab-org/release-cli) tool, passing the tag name `v0.$CI_PIPELINE_IID`. This command will create a tag called with this format.
- Because of the above, a following release jobs that extend the `.release` job template on [a tag pipeline gets automatically triggered](https://gitlab.com/gitlab-org/cluster-integration/helm-install-image/-/blob/f23a3be8c48f50d610c85c455d2a5b1c2af57e53/.gitlab-ci.yml#L96-107). See `rules: \n   - if: $CI_COMMIT_TAG`.
- The above job will create image names with and without `CI_COMMIT_TAG`. See
    ```
    - crane cp "$BUILD_IMAGE_NAME" "${RELEASE_IMAGE_REPOSITORY}:${CI_COMMIT_TAG}-${RELEASE_IMAGE_TAG_SUFFIX}"
    - crane cp "$BUILD_IMAGE_NAME" "${RELEASE_IMAGE_REPOSITORY}:${RELEASE_IMAGE_TAG_SUFFIX}"
    ```

Note that `CI_COMMIT_TAG` is the tag that was created as part of the `prepare_release` job, which at that time got the value of `v0.$CI_PIPELIND_IID`.
